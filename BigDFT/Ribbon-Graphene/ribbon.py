#! /usr/bin/env python

import math
import os
import sys

# opt bond lengths - 1.257, 1.430, (1.390 - constrained by L)

#print "Ce code est dedier a construire le fichier des positions pour..."
#angle=30.0

def carbon_hexagon(anchor,bonds,angle=30.0):
    """
    Defines a set of positions of six carbon atoms starting from a triplet of position (anchor)
    Params: bonds  - two internal bonds of the hexagon
            angle - angle of the hexagon (in degrees)
    """
    import math
    posx1,posy1,posz1=anchor
    bondi1,bondi2=bonds

    posx2=posx1-bondi2*math.cos(math.radians(angle))
    posx3=posx1+bondi2*math.cos(math.radians(angle))
    posx4=posx2
    posx5=posx3
    posx6=posx1

    posy2=posy1
    posy3=posy1
    posy4=posy1
    posy5=posy1
    posy6=posy1

    posz2=posz1+bondi2*math.sin(math.radians(angle))
    posz3=posz2
    posz4=posz2+bondi1
    posz5=posz4
    posz6=posz5+bondi2*math.sin(math.radians(angle))
    
    #create the list of the atomic positions of the hexagon
    return [[posx1,posy1,posz1],[posx2,posy2,posz2],[posx3,posy3,posz3],\
	[posx4,posy4,posz4],[posx5,posy5,posz5],[posx6,posy6,posz6]]

def hexagon_length(bonds,bonde,angle=30.0):
    import math
    bondi1,bondi2=bonds
    return bondi1+2.0*bondi2*math.sin(math.radians(angle))+bonde

def write_xyz(unit,listpos,label=None):
    """
    Writes the atomic position of the atoms in list as carbon atoms
    """
    for x,y,z in listpos:
	lbl='' if label is None else label
	unit.write("C"+lbl+"   "+str(x)+"   "+str(y)+"   "+str(z) + "\n")

if __name__ == '__main__':
	if len(sys.argv) == 1:
	      #fichier1 = raw_input("\nEntrez le nom du fichier souhaite:\n")
	      num  = raw_input("\nEnter the number of benzene rings:\n")
	      bondi1 = raw_input("\nEnter the intra-benzene bond length(1):\n")
	      bondi2 = raw_input("\nEnter the intra-benzene bond length(2):\n")
	      bonde = raw_input("\nEnter the inter-benzene bond length:\n")
	      angle = raw_input("\nEnter the internal bond angle:\n")
	      width = raw_input("\nEnter the x-y box width:\n")
	elif len(sys.argv) == 7:
	     #print "Avec un seul argument, celui-ci est supposer etre le nombre d'atom de Carbone."
	     #print "Alkane ecrit dans le fichier posinp.xyz"
	     num = sys.argv[1]
	     bondi1 = sys.argv[2]
	     bondi2 = sys.argv[3]
	     bonde = sys.argv[4]
	     angle = sys.argv[5]
	     width = sys.argv[6]
	else:
	     print "Wrong number of inputs"
	     print "..."
	     sys.exit()

	fichier1 = 'ribbon'+num+'_labelled.xyz'
	fichier2 = 'ribbon'+num+'.xyz'
	writer1 = open(fichier1,"w")
	writer2 = open(fichier2,"w")

	print "La chaine d'alkane est placee selon la direction Z...."

	# Write the header
	length=hexagon_length([bondi1,bondi2],bonde,angle) #eval(bondi1)+2.0*eval(bondi2)*math.sin(math.radians(eval(angle)))+eval(bonde)
	writer1.write(str(6*eval(num))+" "+"angstroem"+"   #"+str(num)+"   "+str(bondi1)+"   "+str(bondi2)+"   "+str(bonde)+"   "+str(angle)+"   "+str(width)+"\n")
	writer1.write("periodic"+"   "+str(width)+"   "+str(width)+"   "+str(length*eval(num))+"\n")
	writer2.write(str(6*eval(num))+" "+"angstroem"+"   #"+str(num)+"   "+str(bondi1)+"   "+str(bondi2)+"   "+str(bonde)+"   "+str(angle)+"   "+str(width)+"\n")
	writer2.write("periodic"+"   "+str(width)+"   "+str(width)+"   "+str(length*eval(num))+"\n")
	#print 'C_'+nombreC+'H_'+str(2*eval(nombreC) + 2)+"    alkane chain"

	# Write the coordinates of the Carbon Atoms
	#Write the coordinates of the Hydrogene Atoms
	posx1 = 0.5*eval(width)
	posy1 = 0.5*eval(width)
	posz1 = 0.5*eval(bonde)

	for l in range(eval(num)):
	    carbon_hexagon([posx1,posy1,posz1],[bondi1,bondi2],angle)
	    write_xyz(writer2,listpos)
	    write_xyz(writer1,listpos,label=str(l+1))
	    posz1 += length


